# Ruta absoluta o relativa (cuidado con espacios en blanco al final del path!)
ARTOOLKITDIR = ../../ARToolKit
INC_DIR      = ../../include
LIB_DIR      = ../../lib

LIBS   = -lARgsub_lite -lARgsub -lARvideo -lAR -lglut -lGLU -lGL -lm -lalut -lopenal
NAMEEXEC     = npatrones

all: $(NAMEEXEC)

$(NAMEEXEC): $(NAMEEXEC).c 
	cc -I $(INC_DIR) -o $(NAMEEXEC) $(NAMEEXEC).c -L$(LIB_DIR) $(LIBS) 
clean:
	rm -f *.o $(NAMEEXEC) *~ *.*~

