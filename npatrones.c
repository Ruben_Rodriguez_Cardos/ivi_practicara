#include <GL/glut.h>    
#include <AR/gsub.h>    
#include <AR/video.h>   
#include <AR/param.h>   
#include <AR/ar.h>
#include <math.h>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

// ==== Definicion de estructuras ===================================
struct TObject{
  int id;                      // Identificador del patron
  int visible;                 // Es visible el objeto?
  double width;                // Ancho del patron
  double center[2];            // Centro del patron  
  double patt_trans[3][4];     // Matriz asociada al patron
  void (* drawme)(void);       // Puntero a funcion drawme
};

struct TObject *objects = NULL;
int nobjects = 0;

void print_error (char *error) {  printf("%s\n", error); exit(0); }


//OpenAL=============================================================
ALuint Buffer;
ALuint Source;
ALfloat SourcePos[] = { 0.0, 0.0, 0.0 };
ALfloat SourceVel[] = { 0.0, 0.0, 0.0 };
ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };
ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };
ALfloat ListenerOri[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };
int op = 0;
int playing = 0;
ALint state;
//===================================================================

//OpenAL Functions===================================================
ALboolean LoadALData2()
{
	// Variables to load into.

	ALenum format;
	ALsizei size;
	ALvoid* data;
	ALsizei freq;
	ALboolean loop;

	// Load wav data into a buffer.

	alGenBuffers(1, &Buffer);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;

    alutLoadWAVFile((ALbyte*)"Sounds/01.wav", &format, &data, &size, &freq, &loop);
   
	//alutLoadWAVFile((ALbyte*)"wavdata/Effect.wav", &format, &data, &size, &freq, &loop);
	alBufferData(Buffer, format, data, size, freq);
	alutUnloadWAV(format, data, size, freq);

	// Bind the buffer with the source.

	alGenSources(1, &Source);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;

	alSourcei (Source, AL_BUFFER,   Buffer   );
	alSourcef (Source, AL_PITCH,    1.0      );
	alSourcef (Source, AL_GAIN,     1.0      );
	alSourcefv(Source, AL_POSITION, SourcePos);
	alSourcefv(Source, AL_VELOCITY, SourceVel);
	alSourcei (Source, AL_LOOPING,  loop     );

	// Do another error check and return.

	if(alGetError() == AL_NO_ERROR)
		return AL_TRUE;

	return AL_FALSE;
}

//OpenAL Functions===================================================
ALboolean LoadALData(int rot, int note)
{
	// Variables to load into.

	ALenum format;
	ALsizei size;
	ALvoid* data;
	ALsizei freq;
	ALboolean loop;

	// Load wav data into a buffer.

	alGenBuffers(1, &Buffer);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;


	switch(rot) {
      case 0 : // SONIDOS 01 - 07
      	switch(note){
      		case 0:
      			alutLoadWAVFile((ALbyte*)"Sounds/01.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 1:
      			alutLoadWAVFile((ALbyte*)"Sounds/02.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 2:
      			alutLoadWAVFile((ALbyte*)"Sounds/03.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 3:
      			alutLoadWAVFile((ALbyte*)"Sounds/04.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 4:
      			alutLoadWAVFile((ALbyte*)"Sounds/05.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 5:
      			alutLoadWAVFile((ALbyte*)"Sounds/06.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 6:
      			alutLoadWAVFile((ALbyte*)"Sounds/07.wav", &format, &data, &size, &freq, &loop);
      			break;
      		default:
      			printf("Error \n");
      	}
        break;
      case 1: // SONIDOS 08 - 14
      	switch(note){
      		case 0:
      			alutLoadWAVFile((ALbyte*)"Sounds/08.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 1:
      			alutLoadWAVFile((ALbyte*)"Sounds/09.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 2:
      			alutLoadWAVFile((ALbyte*)"Sounds/10.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 3:
      			alutLoadWAVFile((ALbyte*)"Sounds/11.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 4:
      			alutLoadWAVFile((ALbyte*)"Sounds/12.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 5:
      			alutLoadWAVFile((ALbyte*)"Sounds/13.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 6:
      			alutLoadWAVFile((ALbyte*)"Sounds/14.wav", &format, &data, &size, &freq, &loop);
      			break;
      		default:
      			printf("Error \n");
      	}
      	break;
      case 2 : // SONIDOS 15 - 21
        switch(note){
      		case 0:
      			alutLoadWAVFile((ALbyte*)"Sounds/15.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 1:
      			alutLoadWAVFile((ALbyte*)"Sounds/16.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 2:
      			alutLoadWAVFile((ALbyte*)"Sounds/17.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 3:
      			alutLoadWAVFile((ALbyte*)"Sounds/18.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 4:
      			alutLoadWAVFile((ALbyte*)"Sounds/19.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 5:
      			alutLoadWAVFile((ALbyte*)"Sounds/20.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 6:
      			alutLoadWAVFile((ALbyte*)"Sounds/21.wav", &format, &data, &size, &freq, &loop);
      			break;
      		default:
      			printf("Error \n");
      	}
        break;
      case 3: // SONIDOS 22 - 28
      	switch(note){
      		case 0:
      			alutLoadWAVFile((ALbyte*)"Sounds/22.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 1:
      			alutLoadWAVFile((ALbyte*)"Sounds/23.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 2:
      			alutLoadWAVFile((ALbyte*)"Sounds/24.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 3:
      			alutLoadWAVFile((ALbyte*)"Sounds/25.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 4:
      			alutLoadWAVFile((ALbyte*)"Sounds/26.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 5:
      			alutLoadWAVFile((ALbyte*)"Sounds/27.wav", &format, &data, &size, &freq, &loop);
      			break;
      		case 6:
      			alutLoadWAVFile((ALbyte*)"Sounds/28.wav", &format, &data, &size, &freq, &loop);
      			break;
      		default:
      			printf("Error \n");
      	}
      	break;
      default :
        printf("Invalid op\n" );
   	}
   
	//alutLoadWAVFile((ALbyte*)"wavdata/Effect.wav", &format, &data, &size, &freq, &loop);
	alBufferData(Buffer, format, data, size, freq);
	alutUnloadWAV(format, data, size, freq);

	// Bind the buffer with the source.

	alGenSources(1, &Source);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;

	alSourcei (Source, AL_BUFFER,   Buffer   );
	alSourcef (Source, AL_PITCH,    1.0      );
	alSourcef (Source, AL_GAIN,     1.0      );
	alSourcefv(Source, AL_POSITION, SourcePos);
	alSourcefv(Source, AL_VELOCITY, SourceVel);
	alSourcei (Source, AL_LOOPING,  loop     );

	// Do another error check and return.

	if(alGetError() == AL_NO_ERROR)
		return AL_TRUE;

	return AL_FALSE;
}

void SetListenerValues()
{
	alListenerfv(AL_POSITION,    ListenerPos);
	alListenerfv(AL_VELOCITY,    ListenerVel);
	alListenerfv(AL_ORIENTATION, ListenerOri);
}

void KillALData()
{
	alDeleteBuffers(1, &Buffer);
	alDeleteSources(1, &Source);
	alutExit();
}
//===================================================================

// ==== addObject (Anade objeto a la lista de objetos) ==============
void addObject(char *p, double w, double c[2], void (*drawme)(void)) 
{
  int pattid;

  if((pattid=arLoadPatt(p)) < 0) 
    print_error ("Error en carga de patron\n");

  nobjects++;
  objects = (struct TObject *) 
    realloc(objects, sizeof(struct TObject)*nobjects);

  objects[nobjects-1].id = pattid;
  objects[nobjects-1].width = w;
  objects[nobjects-1].center[0] = c[0];
  objects[nobjects-1].center[1] = c[1];
  objects[nobjects-1].drawme = drawme;
}

// ==== draw****** (Dibujado especifico de cada objeto) =============
void drawteapot(void) {
  GLfloat material[]     = {0.0, 0.0, 1.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glTranslatef(0.0, 0.0, 60.0);
  glRotatef(90.0, 1.0, 0.0, 0.0);
  glutSolidTeapot(80.0);
}

void drawcube(void) {
  GLfloat material[]     = {1.0, 0.0, 0.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glTranslatef(0.0, 0.0, 40.0);
  glutSolidCube(80.0);
}

// ======== cleanup =================================================
static void cleanup(void) {   // Libera recursos al salir ...
  arVideoCapStop();  arVideoClose();  argCleanup();  free(objects);  
  exit(0);
}

// ======== keyboard ================================================
static void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 0x1B: case 'Q': case 'q':
    cleanup(); break;
  }
}

// ======== draw ====================================================
void draw( void ) {
  double  gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
  GLfloat light_position[]  = {100.0,-200.0,200.0,0.0};
  int i;
  
  argDrawMode3D();              // Cambiamos el contexto a 3D
  argDraw3dCamera(0, 0);        // Y la vista de la camara a 3D
  glClear(GL_DEPTH_BUFFER_BIT); // Limpiamos buffer de profundidad
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  for (i=0; i<nobjects; i++) {
    if (objects[i].visible) {   // Si el objeto es visible
      argConvGlpara(objects[i].patt_trans, gl_para);   
      glMatrixMode(GL_MODELVIEW);           
      glLoadMatrixd(gl_para);   // Cargamos su matriz de transf.            

      glEnable(GL_LIGHTING);  glEnable(GL_LIGHT0);
      glLightfv(GL_LIGHT0, GL_POSITION, light_position);
      objects[i].drawme();      // Llamamos a su función de dibujar
    }
  }
  glDisable(GL_DEPTH_TEST);
}

// ======== init ====================================================
static void init( void ) {
  ARParam  wparam, cparam;   // Parametros intrinsecos de la camara
  int xsize, ysize;          // Tamano del video de camara (pixels)
  double c[2] = {0.0, 0.0};  // Centro de patron (por defecto)
  
  //OpenAL===========================================================
  alutInit(NULL, 0);
  alGetError();

  if(LoadALData2() == AL_FALSE)
  {
    printf("Error loading data.");
    return 0;
  }

  //=================================

  SetListenerValues();
  atexit(KillALData);
  //=================================================================


  // Abrimos dispositivo de video
  if(arVideoOpen("-dev=/dev/video0") < 0) exit(0);  
  if(arVideoInqSize(&xsize, &ysize) < 0) exit(0);

  // Cargamos los parametros intrinsecos de la camara
  if(arParamLoad("data/camera_para.dat", 1, &wparam) < 0)   
    print_error ("Error en carga de parametros de camara\n");
  
  arParamChangeSize(&wparam, xsize, ysize, &cparam);
  arInitCparam(&cparam);   // Inicializamos la camara con "cparam"

  // Inicializamos la lista de objetos
  addObject("data/simple.patt", 120.0, c, drawteapot); 
  addObject("data/identic.patt", 90.0, c, drawcube); 

  argInit(&cparam, 1.0, 0, 0, 0, 0);   // Abrimos la ventana 
}

// ======== mainLoop ================================================
static void mainLoop(void) {
  ARUint8 *dataPtr;
  ARMarkerInfo *marker_info;
  int marker_num, i, j, k;

  //OpenAL======================================
  alGetSourcei(Source, AL_SOURCE_STATE, &state);
  if(AL_PLAYING == state){
    playing = 1;
  }else{
    playing = 0;
  }
  //============================================

  // Capturamos un frame de la camara de video
  if((dataPtr = (ARUint8 *)arVideoGetImage()) == NULL) {
    // Si devuelve NULL es porque no hay un nuevo frame listo
    arUtilSleep(2);  return;  // Dormimos el hilo 2ms y salimos
  }

  argDrawMode2D();
  argDispImage(dataPtr, 0,0);    // Dibujamos lo que ve la camara 

  // Detectamos la marca en el frame capturado (return -1 si error)
  if(arDetectMarker(dataPtr, 100, &marker_info, &marker_num) < 0) {
    cleanup(); exit(0);   // Si devolvio -1, salimos del programa!
  }

  arVideoCapNext();      // Frame pintado y analizado... A por otro!

  // Vemos donde detecta el patron con mayor fiabilidad
  for (i=0; i<nobjects; i++) {
    for(j = 0, k = -1; j < marker_num; j++) {
      if(objects[i].id == marker_info[j].id) {
	if (k == -1) k = j;
	else if(marker_info[k].cf < marker_info[j].cf) k = j;
      }
    }
    
    if(k != -1) {   // Si ha detectado el patron en algun sitio...
      objects[i].visible = 1;
      arGetTransMat(&marker_info[k], objects[i].center, 
		    objects[i].width, objects[i].patt_trans);
    } else { objects[i].visible = 0; }  // El objeto no es visible
  }

  //CALCULO

  //SI TODAS SON VISIBLES HAGO COSAS
  int all_visible = 1;
  for (i=0; i<nobjects; i++) {
  	if(objects[i].visible == 0){
  		all_visible = 0;
  	}
  }

  if(all_visible == 1){
	//printf("%s\n", "Todas las marcas son visibles");

  	//Rotacion de la marca 1 CUBO
  	int rot = -1;
  	int id;
  	rot=marker_info[objects[1].id].dir;
  	op = rot;


  	//Angulo de la marca 2 TETERA
  	float angle=0.0, module=0.0;
  	double v[3];
  	v[0] = objects[0].patt_trans[0][0]; 
    v[1] = objects[0].patt_trans[1][0]; 
    v[2] = objects[0].patt_trans[2][0]; 
      
    module = sqrt(pow(v[0],2)+pow(v[1],2)+pow(v[2],2));
    v[0] = v[0]/module;  v[1] = v[1]/module; v[2] = v[2]/module; 
    angle = acos (v[0]) * 57.2958;   // Sexagesimales! * (180/PI)

    //Segun el angulo calculamos la nota=====
    //Intervalo 10 - 170
    int note = angle/26;

    //=======================================

	//Distancia entre las marcas
	int distance = -1; //limites 180 y 500
	int dx = objects[1].patt_trans[0][3] - objects[0].patt_trans[0][3];
	int dy = objects[1].patt_trans[1][3] - objects[0].patt_trans[1][3];
	int dz = objects[1].patt_trans[2][3] - objects[0].patt_trans[2][3];
	distance = sqrt((dx*dx)+(dy*dy)+(dz*dz));

	//En funcion de la distancia calculo el volumen===
	float newVolume = 0.5f;
	
	if(distance > 180 && distance < 500){
		newVolume = (180 + (500 - 180) * distance)/100000.0;
	}
	if(distance <= 180){
		newVolume = 0.1f;
	}
	if(distance >= 500){
		newVolume = 1.0;
	}
			
	//OpenAL=================
	if(playing == 0){
		LoadALData(rot, note);
		alSourcef(Source, AL_GAIN, newVolume);
	    alSourcePlay(Source);
	}
	//=======================

	//================================================

	printf(" Rotacion: %d  Distancia: %d Angulo: %f Volumen: %f Note: %d \n",rot ,distance, angle, newVolume, note);


  }

 
  draw();           // Dibujamos los objetos de la escena
  argSwapBuffers(); // Cambiamos el buffer con lo que tenga dibujado
}

// ======== Main ====================================================
int main(int argc, char **argv) {
  glutInit(&argc, argv);    // Creamos la ventana OpenGL con Glut
  init();                   // Llamada a nuestra funcion de inicio
  
  arVideoCapStart();        // Creamos un hilo para captura de video
  argMainLoop( NULL, keyboard, mainLoop );    // Asociamos callbacks
  return (0);
}
